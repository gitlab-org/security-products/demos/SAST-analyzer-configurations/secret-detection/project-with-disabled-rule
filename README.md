### ALL SECRETS IN THIS PROJECT ARE FAKE DUMMY SECRETS ONLY USED TO ILLUSTRATE PIPELINE SECRET DETECTION [CUSTOMIZE RULESETS](https://docs.gitlab.com/user/application_security/secret_detection/pipeline/configure/#customize-analyzer-rulesets) FEATURE.

# Disable a rule from default ruleset using a remote ruleset

This project [disables a rule from the default ruleset configuration](https://docs.gitlab.com/user/application_security/secret_detection/pipeline/configure/#disable-a-rule) of pipeline secret detection using a [remote ruleset](https://docs.gitlab.com/user/application_security/secret_detection/pipeline/configure/#with-a-remote-ruleset) to ensure the rule is no being longer detected.

The result of running `secret_detection` job in the pipelines of this project is:

1. A *[**remote ruleset**](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/modify-default-ruleset/remote-ruleset/disable-rule-ruleset/-/blob/main/.gitlab/secret-detection-ruleset.toml) is used to disable a rule from the [default ruleset](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/gitleaks.toml).
1. A [secret](./private_key) is no longer detected in the `gl-secret-detection-report.json` file.

*A _remote ruleset_ is a **ruleset configuration file stored outside the current repository**.

To confirm this working as intended, check the [security tab of one of the successful pipelines](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/modify-default-ruleset/remote-ruleset/disable-rule-project/-/pipelines/1380720096/security).
